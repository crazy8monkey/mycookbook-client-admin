import api from './api.service';
const apiUrl = "/error";

class ErrorService {
  List() {
    return api.get(apiUrl).then((resp) => resp.data);
  }
  Get(id) {
    return api.get(`${apiUrl}/${id}`).then((resp) => resp.data);
  }
  Update(id, data) {
    return api.put(`${apiUrl}/${id}`, data).then((resp) => resp.data)
  }
  Remove(id) {
    return api.delete(`${apiUrl}/${id}`)
  }
}

export default new ErrorService();
