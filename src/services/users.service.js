import api from './api.service';
const apiUrl = "/users";

class UserService {
  List() {
    return api.get(apiUrl).then((resp) => resp.data);
  }
  Get(id) {
    return api.get(`${apiUrl}/${id}`).then((resp) => resp.data)
  }
  Remove(id) {
    return api.delete(`${apiUrl}/${id}`)
  }
}

export default new UserService();
