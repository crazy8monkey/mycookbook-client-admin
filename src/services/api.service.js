import axios from "axios"
import TokenService from './token.service';

const instance = axios.create({
  baseURL: "http://localhost:3000/api",
  headers: {
    "Content-Type": "application/json",
  },
})

//need to add logic where endpoints don't need token
instance.interceptors.request.use(
  (config) => {
    const token = TokenService.getAccessToken();

    if(token) {
      config.headers['Authorization'] = `Bearer ${token}`
    }

    return config
  },
  (error) => {

    console.log("error request => ", error);
    return Promise.reject(error);
  }
);


instance.interceptors.response.use(
  res => res,
  async (err) => {
    const originalConfig = err.config;
    if(originalConfig.url !== "/profile/login" &&  err.response) {
      if (err.response.status === 401 && !originalConfig._retry) {
        originalConfig._retry = true;

        try {
          const rs = await TokenService.newRefreshToken();
          const { token, refresh_token } = rs;
          TokenService.setAccessToken(token);
          TokenService.setRefreshToken(token);

          instance.defaults.headers['Authorization'] = `Bearer ${token}`

          return instance(originalConfig);
        } catch (_error) {
          return Promise.reject(_error);
        }
      }
    }

    return Promise.reject(err);
  }
);

export default instance;
