//react
import React from 'react';
import { Link } from 'react-router-dom';
//styles
import './users-get.component.styles.scss';
//services
import UserService from '../../services/users.service';
//components
import HeaderComponent from '../../components/header/header.component';

class UsersGet extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      user: null
    }
  }


  componentDidMount() {
    const { id } = this.props.match.params;
    UserService.Get(id).then((user) => {
      this.setState({ user });
    });
  }

  render() {
    const { user } = this.state;
    return(
      <React.Fragment>
        <HeaderComponent />
        <div className="container-fluid" style={{ paddingTop: "20px", paddingLeft:"20px", paddingRight: "20px"}}>
          <div className="row">
            <div className="col">
              <nav aria-label="breadcrumb">
                <ol className="breadcrumb">
                  <li className="breadcrumb-item">
                    <Link to="/users">Users</Link>
                  </li>
                  <li className="breadcrumb-item active" aria-current="page">Viewing User</li>
                </ol>
              </nav>
            </div>
          </div>
          <div className="whiteBox">
            <div className="content">
              <div className="row">
                <div className="col">
                  <div className="label">Full name</div>
                  <div>{user?.first_name} {user?.last_name}</div>
                </div>
                <div className="col">
                  <div className="label">Email</div>
                  <div>{user?.email}</div>
                </div>
              </div>
              <div className="row">
                <div className="label">Date Signed up</div>
                <div>{user?.registered_date}</div>
              </div>
            </div>
          </div>
        </div>
      </React.Fragment>
    )
  }
}

export default UsersGet;
