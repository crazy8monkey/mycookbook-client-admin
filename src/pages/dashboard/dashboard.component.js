//react
import React from 'react';

import './dashboard.component.styles.scss';

import HeaderComponent from '../../components/header/header.component';

class DashboardPage extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return(
      <React.Fragment>
        <HeaderComponent />
        <div>This is the home/dashboard page, will be implemented in the future</div>
      </React.Fragment>
    )
  }
}

export default DashboardPage;
