//react
import React from 'react';
import { Link } from 'react-router-dom';
//styles
import './error-view.component.styles.scss';
//components
import HeaderComponent from '../../components/header/header.component';
//services
import ErrorService from '../../services/errors.service';
import ErrorCategoryService from '../../services/error-category.service';
//misc
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

class ViewError extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      error: null,
      categories:[]
    }
  }


  componentDidMount() {
    const { id } = this.props.match.params
    ErrorService.Get(id).then((error) => {
      this.setState({ error });
    });

    ErrorCategoryService.List().then((categories) => {
      this.setState({ categories });
    })
  }

  categoryChange = e => {
    const { error } = this.state;
    console.log("category change => ", e.target.value);
    this.setState({ error: {
        ...error,
        category: e.target.value
      }
    });
  }

  saveError = async e => {
    e.preventDefault();
    const { error } = this.state;
    let updateError = { category: error.category }
    const { id } = this.props.match.params;
    ErrorService.Update(id, updateError).then((update) => {
      toast.success("Error updated", { position: "bottom-center" });
    });
  }

  render() {
    const { error, categories } = this.state;
    return(
      <React.Fragment>
        <HeaderComponent />
        <div className="container-fluid" style={{ paddingTop: "20px", paddingLeft:"20px", paddingRight: "20px"}}>
          <div className="row">
            <div className="col">
              <nav aria-label="breadcrumb">
                <ol className="breadcrumb">
                  <li className="breadcrumb-item">
                    <Link to="/errors/list">Errors</Link>
                  </li>
                  <li className="breadcrumb-item active" aria-current="page">Viewing Error</li>
                </ol>
              </nav>
            </div>
          </div>
          <div className="whiteBox">
            <form onSubmit={this.saveError}>
              <div className="content">
                <div className="row">
                  <div className="col">
                    <div className="errorHeader">Viewing Error #{error?.id}</div>
                  </div>
                </div>
                <div className="row">
                  <div className="col">
                    <div className="label">Submitted Date</div>
                    <div>{error?.submitted_date}</div>
                  </div>
                  <div className="col">
                    <div className="label">Submitted By</div>
                    <div>{error?.first_name} {error?.last_name}</div>
                  </div>
                  <div className="col">
                    <div className="label">Was logged in</div>
                    <div>{error?.user_id != null ? "Yes" : "No"}</div>
                  </div>
                </div>
                <div className="row">
                  <div className="col">
                    <div className="label">Category</div>
                    <div>
                      <select onChange={this.categoryChange} value={error?.category}>
                        {categories.map(category => (
                          <option key={category.id} value={category.id}>{category.name}</option>
                        ))}
                      </select>
                    </div>
                  </div>
                </div>
                <div className="row">
                  <div className="col">
                    <div className="label">Error Description</div>
                    <div dangerouslySetInnerHTML={{ __html: error?.description }}></div>
                  </div>
                </div>
                <div className="row buttonContainer">
                  <div className="col">
                    <button type="submit" className="btn btn-primary">Save</button>
                  </div>
                </div>
              </div>
            </form>

          </div>
        </div>
        <ToastContainer />
      </React.Fragment>
    )

  }
}

export default ViewError;
