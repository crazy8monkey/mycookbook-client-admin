//react
import React from 'react';
import { Link, useLocation } from 'react-router-dom';
//services
import ErrorService from '../../services/errors.service';
import ErrorCategoryService from '../../services/error-category.service';
//styles
import './errors.component.styles.scss';
//components
import HeaderComponent from '../../components/header/header.component';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import * as _ from 'lodash';
import FormInput from '../../components/form-input/form-input.component';


const useQuery = () => new URLSearchParams(useLocation().search);

class ErrorsPage extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      errors: [],
      categories: [],
      category: ''
    }
  }

  componentDidMount() {
    const { errors } = this.state;
    ErrorService.List().then((errors) => {
      this.setState({ errors });
    });

    ErrorCategoryService.List().then((categories) => {
      this.setState({ categories });
    })
  }

  removeTicket = (id, event) => {
    event.stopPropagation();
    const { errors } = this.state;
    let currentErrors = errors;
    ErrorService.Remove(id).then((response) => {
      if(response.success) {
        let errorIndex = _.findIndex(currentErrors, {id: id});
        currentErrors.splice(errorIndex, 1);
        this.setState({ errors: currentErrors });
        toast.dark("Ticket Removed", { position: "bottom-center" });
      }
    });
  }

  removeCategory = (id, event) => {
    event.stopPropagation();
    const { categories } = this.state;
    ErrorCategoryService.Remove(id).then((response) => {
      let currentCategories = categories;
      let categoryIndex = _.findIndex(currentCategories, {id: id});
      currentCategories.splice(categoryIndex, 1);
      this.setState({ categories: currentCategories });
      toast.dark("Category Removed", { position: "bottom-center" });
    });
  }

  handleNewCategory = event => {
    const { value, name } = event.target;
    this.setState({ [name]: value });
  };

  submitCategory = async e => {
    e.preventDefault();
    const { category, categories } = this.state;
    let currentCategories = categories;
    let newCategory = { name: category }
    ErrorCategoryService.Add(newCategory).then((response) => {
        // if(response.success) {
        //     currentCategories.push(response.category);
        //     this.setState({
        //       category: "",
        //       categories: currentCategories
        //     });
        // }
    });
  }

  clickCategory = (evt, category) => {
    const { history } = this.props;
    history.push(`category/${category.id}`);
  }

  clickError = (evt, error) => {
    const { history } = this.props;
    history.push(`view/${error.id}`);
  }

  render() {
    const { errors, categories, category } = this.state;

    return(
      <React.Fragment>
        <HeaderComponent />
        <div className="container-fluid" style={{ paddingTop: "20px", paddingLeft:"20px", paddingRight: "20px"}}>
          <div className="row">
            <div className="col-3">
              <div className="whiteBox">
                <Link to="/errors/list">
                  <div className="sub-link">Errors List</div>
                </Link>
                <Link to="/errors/category">
                  <div className="sub-link">Categories</div>
                </Link>
              </div>
            </div>
            <div className="col">
              {this.props.match.params.type === "list" &&
                <div className="whiteBox">
                  {errors.length > 0 &&
                    <table className="table table-hover">
                      <thead>
                        <tr>
                          <th>Ticket #</th>
                          <th>Time Submitted</th>
                          <th>Full Name</th>
                          <th>Category</th>
                          <th>Logged In</th>
                          <th></th>
                        </tr>
                      </thead>
                      <tbody>
                        {errors.map((error) => (
                          <tr key={error.id} onClick={(event) => this.clickError(event, error)}>
                            <td>
                              {error.id}
                            </td>
                            <td>
                              {error.submitted_date}
                            </td>
                            <td>
                              {error.first_name} {error.last_name}
                            </td>
                            <td>
                              {error.category}
                            </td>
                            <td>
                              {error.user_id != null ? "Yes" : "No"}
                            </td>
                            <td>
                              <i className="fa fa-trash" onClick={(event) => this.removeTicket(error.id, event)} aria-hidden="true"></i>
                            </td>
                          </tr>
                        ))}
                      </tbody>
                    </table>
                  }
                  {errors.length == 0 &&
                    <div className="emptyListContent">There are no errors registered at this time</div>
                  }
                </div>
                }

                {this.props.match.params.type === "category" &&
                  <React.Fragment>
                    <div className="whiteBox">
                      <div className="addingNewCategory">
                        <form onSubmit={this.submitCategory}>
                          <FormInput
                            label="Add New Category"
                            type="text"
                            name="category"
                            handleChange={this.handleNewCategory}
                            value={category}
                          />
                          <button type="submit" className="btn btn-primary">Save</button>
                        </form>

                      </div>
                    </div>
                    <div className="whiteBox" style={{ marginTop: '20px' }}>
                      {categories.length > 0 &&
                        <table className="table table-hover">
                          <thead>
                            <tr>
                              <th>ID</th>
                              <th>Category</th>
                              <th></th>
                            </tr>
                          </thead>
                          <tbody>
                            {categories.map((category) => (
                              <tr key={category.id} onClick={(event) => this.clickCategory(event, category)}>
                                <td>
                                  {category.id}
                                </td>
                                <td>
                                  {category.name}
                                </td>
                                <td>
                                  <i className="fa fa-trash" onClick={(event) => this.removeCategory(category.id, event)} aria-hidden="true"></i>
                                </td>
                              </tr>
                            ))}
                          </tbody>
                        </table>
                      }
                      {categories.length == 0 &&
                        <div className="emptyListContent">There are no Categories entered at this time</div>
                      }
                    </div>
                  </React.Fragment>
                }
            </div>
          </div>
        </div>
        <ToastContainer />
      </React.Fragment>
    )
  }
}

export default ErrorsPage;
