//react
import React from 'react';
//styles
import './users.component.styles.scss';
//services
import UserService from '../../services/users.service';
//component
import HeaderComponent from '../../components/header/header.component';
//misc
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import * as _ from 'lodash';

class UsersPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      users:[]
    }
  }

  componentDidMount() {
    UserService.List().then((users) => {
      this.setState({ users: users });
    });
  }

  userRowOnClick = (userId, event) => {
    const { history } = this.props;
    history.push(`users/${userId}`);
  }

  userDeleteOnClick = (userId, event) => {
    event.stopPropagation();
    const { users } = this.state;
    let currentUsers = users;
    UserService.Remove(userId).then((response) => {
      let userIndex = _.findIndex(currentUsers, {id: userId});
      currentUsers.splice(userIndex, 1);
      this.setState({ users: currentUsers });
      toast.dark("User Removed", { position: "bottom-center" });
    });
  }



  render() {
    const { users } = this.state;
    return(
      <React.Fragment>
        <HeaderComponent />
        <div className="container-fluid">
          <div className="row">
            <div className="col">
              <div className="userListContent">
                <div className="whiteBox">
                  {users.length > 0 &&
                    <table className="table table-hover">
                      <thead>
                        <tr>
                          <th>Full Name</th>
                          <th>Signed Up Date</th>
                          <th></th>
                        </tr>
                      </thead>
                      <tbody>
                        {users.map((user) => (
                          <tr key={user.id} onClick={(event) => this.userRowOnClick(user.id, event)}>
                            <td>
                              {user.first_name} {user.last_name}
                            </td>
                            <td>
                              {user.registered_date}
                            </td>
                            <td><i onClick={(event) => this.userDeleteOnClick(user.id, event)} className="fas fa-trash"></i></td>
                          </tr>
                        ))}
                      </tbody>
                    </table>
                  }
                  {users.length == 0 &&
                    <div className="emptyListContent">There are no users registered at this time</div>
                  }
                </div>
              </div>
            </div>
          </div>
        </div>
        <ToastContainer />
      </React.Fragment>
    )
  }
}

// <div key={user.id}><
// <option key={user.id} value={category.id}>{category.name}</option>

export default UsersPage;
