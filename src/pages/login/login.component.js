//react
import React from 'react';
import { Link } from 'react-router-dom';
//styles
import './login.component.styles.scss';
//services
import LoginService from '../../services/login.service';
import TokenService from '../../services/token.service';
//component
import FormInput from '../../components/form-input/form-input.component';
//services
import ErrorCategoryService from '../../services/error-category.service';

class LoginPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: ''
    }
  }

  handleChange = event => {
    const { value, name } = event.target;
    // const { errors } = this.state;
    this.setState({ [name]: value });
  };

  loginUser = async e => {
    e.preventDefault();
    const { email, password } = this.state;
    const { history } = this.props;
    const login = {
      email,
      password,
      role: "Admin"
    }
    LoginService.Login(login).then((response) => {
      TokenService.setAccessToken(response.token);
      TokenService.setRefreshToken(response.refresh_token);
      history.push("/home");
    });
  }


  render() {
    const { email, password } = this.state;
    return(
      <React.Fragment>
        <div class="container-fluid">
          <div className="login-form-container">
            <div className="content">
              <form onSubmit={this.loginUser}>
                <div className="row">
                  <div className="col">
                    <FormInput
                      label="Email"
                      type="email"
                      name="email"
                      handleChange={this.handleChange}
                      value={email}
                    />
                  </div>
                </div>
                <div className="row">
                  <div className="col">
                    <FormInput
                      label="Password"
                      type="password"
                      name="password"
                      handleChange={this.handleChange}
                      value={password}
                    />
                  </div>
                </div>
                <div className="row">
                  <div className="col">
                    <button type="submit" className="btn btn-primary">Login</button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>

      </React.Fragment>

    )
  }
}

export default LoginPage;
