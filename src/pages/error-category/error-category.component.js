//react
import React from 'react';
import { Link } from 'react-router-dom';
//services
import ErrorCategoryService from '../../services/error-category.service';
//components
import HeaderComponent from '../../components/header/header.component';
import FormInput from '../../components/form-input/form-input.component';
//misc
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

class ErrorsCategory extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      name: null
    }
  }

  componentDidMount() {
    const { id } = this.props.match.params;
    ErrorCategoryService.Get(id).then((category) => {
      this.setState({name: category.name});
    })
  }

  handleChange = event => {
    const { value, name } = event.target;
    const { errors } = this.state;
    this.setState({ [name]: value });
  };

  submitCategory = async e => {
    e.preventDefault();
    const { name } = this.state;
    const { id } = this.props.match.params;
    let updateCategory = { name }
    ErrorCategoryService.Update(id, updateCategory).then((response) => {
      toast.success("Category Updated", { position: "bottom-center" });
    });
  }

  render() {
    const { name } = this.state;

    return(
      <React.Fragment>
        <HeaderComponent />
        <div className="container-fluid" style={{ paddingTop: "20px", paddingLeft:"20px", paddingRight: "20px"}}>
          <div className="row">
            <div className="col">
              <nav aria-label="breadcrumb">
                <ol className="breadcrumb">
                  <li className="breadcrumb-item">
                    <Link to="/errors/category">Categories</Link>
                  </li>
                  <li className="breadcrumb-item active" aria-current="page">Viewing Category</li>
                </ol>
              </nav>
            </div>
          </div>
          <div className="row">
            <div className="col">
              <div className="whiteBox">
                <div className="formContent">
                  <form onSubmit={this.submitCategory}>
                    <FormInput
                      label="Category Name"
                      type="text"
                      name="name"
                      handleChange={this.handleChange}
                      value={name}
                    />
                    <button type="submit" className="btn btn-primary">Save</button>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
        <ToastContainer />
      </React.Fragment>
    )
  }
}

export default ErrorsCategory;
