import logo from './logo.svg';
import './App.scss';
import { Route, Switch } from 'react-router-dom';

import LoginPage from './pages/login/login.component';
import DashboardPage from './pages/dashboard/dashboard.component';
import UsersPage from './pages/users/users.component';
import UsersGet from './pages/users-get/users-get.component';
import ErrorsPage from './pages/errors/errors.component';
import ErrorsCategory from './pages/error-category/error-category.component';
import ViewError from './pages/error-view/error-view.component';
import GuardedRoute  from './components/guarded-route/guarded-route.component';



function App() {
  return (
    <Switch>
      <GuardedRoute exact path='/errors/view/:id'  component={ViewError} />
      <GuardedRoute exact path='/errors/category/:id'  component={ErrorsCategory} />
      <GuardedRoute exact path='/errors/:type'  component={ErrorsPage} />
      <GuardedRoute exact path='/users'  component={UsersPage} />
      <GuardedRoute exact path='/users/:id'  component={UsersGet} />
      <GuardedRoute exact path='/home'  component={DashboardPage} />
      <Route exact path="/" component={LoginPage} />
    </Switch>
  );
}

export default App;
