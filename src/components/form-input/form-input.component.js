//react
import React from 'react';
//styles
import './form-input.component.styles.scss';

const FormInput = ({ handleChange, label, type, name, value }) => (
  <div className="inputLine">
      {label}
      <input type={type} name={name} onChange={handleChange} value={value}/>

  </div>
);

export default FormInput;
