//react
import React, { useState } from 'react'
import { Link, Redirect } from 'react-router-dom';
//styles
import './header.component.styles.scss';

const HeaderComponent = () => {
  return (
    <header>
      <div className="content">
        <div className="left">Home</div>
        <div className="right">
          <Link to="/users">
            <div className="link">Users</div>
          </Link>
          <Link to="/errors/list">
            <div className="link">Errors</div>
          </Link>
          <div className="link">Logout</div>
        </div>

      </div>

    </header>
  )
};


export default HeaderComponent;
